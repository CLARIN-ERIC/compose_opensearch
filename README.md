# OpenSearch compose project for the CLARIN infrastructure

This project was designed to support the deployment of an instance of OpenSearch
and OpenSearch dashboard for the purpose of log aggregation and visualisation.

## Deployment

Deploy as any CLARIN compose project – see
[deploy script](https://gitlab.com/CLARIN-ERIC/deploy-script)
and
[control script](https://gitlab.com/CLARIN-ERIC/control-script/).

Follow the instruction below to __configure the project before starting it__.

## Configuration

Deployment without overlays require no setting of environment variables. The
sections below introduce the overlays and how they are configured.

### Overlays

#### nginx

This overlay enables an nginx proxy that exposes the OpenSearch Dashboard
but restricts access by means of basic authentication.

To enable the overlay, add to `.overlays`:

```text
nginx
```

Set one ore more passwords in a file with the right permissions to allow it
to be mounted into the docker container. For example:

```sh
mkdir -p /home/deploy/opensearch/nginx && \
docker run -ti --rm httpd:2.4 htpasswd -bnBC 10 'admin' 'my_p4ssw0rd' >> '/home/deploy/opensearch/nginx/htpasswd'
```

Check the file's content. Then, assuming the location of the htpasswd file from
the example above, add to `.env`:

```text
NGINX_HTPASSWD_FILE=/home/deploy/opensearch/nginx/htpasswd
NGINX_HTTPS_PORT=9543
```

Or set any other available port for incoming HTTPS traffic to the OpenSearch 
dashboard.

#### fluentd

This overlay provides an instance of fluentd that can capture logs and
forward them to OpenSearch using [Fluent::Plugin::OpenSearch](https://github.com/fluent/fluent-plugin-opensearch)
through the [docker-fluent-opensearch](https://gitlab.com/CLARIN-ERIC/docker-fluent-opensearch)
image.

To enable the overlay, add to `.overlays`:

```text
fluentd
```

Create a private key with a password and a certificate to be used for the "secure
forward" feature of fluent. In the samples below we will assume that there
are two files:

* a certificate file `/home/deploy/opensearch/fluent/certificates/ca_cert.pem`
* a password protected private key file `/home/deploy/opensearch/fluent/certificates/ca_key.pem`

Copy the sample configuration file to a readable path outside the compose
project, e.g.:

```sh
cp clarin/fluent/example/fluentd.conf /home/deploy/opensearch/fluent/fluentd.conf
```

Edit the newly created file, paying attention in particular to the shared key
for transmission of log files (which must be matched on the sending side), and
the password for the certificate's private key (see above).

Add the following to `.env`:

```text
FLUENT_CONF_FILE=/home/deploy/opensearch/fluent/fluentd.conf
FLUENT_SECURE_CERTIFICATE_DIR=/home/deploy/opensearch/fluent/certificates
#FLUENT_PORT=24284
```

Overriding the port is optional. Make sure it matches the port set in the
fluentd configuration.

#### mail

This overlay connects the OpenSearch containers to the `postfix_mail` network,
which is assumed to exist as a docker network external to the project.

To enable the overlay, add to `.overlays`:

```text
mail
```

There are no configuration options or environment variables specific to this
overlay.
